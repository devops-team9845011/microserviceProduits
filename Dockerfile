FROM openjdk:8-jdk-alpine
ADD target/*.jar microserviceproduits.jar
ENTRYPOINT ["java","-jar","/microserviceproduits.jar"]
 
